package com.example.servicioproductos.models.service;

import com.example.servicioproductos.models.entity.Producto;

import java.util.List;

public interface IProductoService {
    public List<Producto> findAll();
    public Producto findById(Long id);
}
